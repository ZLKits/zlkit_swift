//
//  ZLNavigationBackgroundBar.swift
//  ZLKit_Swift
//
//  Created by itzhaolei on 2022/1/24.
//

import UIKit

extension ZLNavigation {
    
    class BackgroundBar: UIView {
                
        override init(frame: CGRect) {
            super.init(frame: frame)
            addSubviews()
            addConstraints()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
    
}

// 添加子视图
extension ZLNavigation.BackgroundBar {
    
    private func addSubviews() {
        
    }
    
}

// 添加子视图约束
extension ZLNavigation.BackgroundBar {
    
    private func addConstraints() {
        
    }
    
}

// 事件区
extension ZLNavigation.BackgroundBar {
    
    
    
}
