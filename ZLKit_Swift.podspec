
Pod::Spec.new do |s|
    s.name             = 'ZLKit_Swift'

    s.version          = '3.0.0'

    s.summary          = '常用组件库'

    s.description      = <<-DESC
        TODO: Add long description of the pod here.
                       DESC

    s.homepage         = 'https://gitee.com/ZLKits/zlkit_swift'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'itzhaolei' => 'itzhaolei@foxmail.com' }
    s.source           = { :git => 'https://gitee.com/ZLKits/zlkit_swift.git', :tag => s.version }

    s.ios.deployment_target = '9.0'
    
    # 该段中的三个header均是用来尝试桥接oc文件的，但是尝试失败了，并未达到效果。
    #s.private_header_files = 'ZLKit_SwiftDemo/Classes/Bridging-Header.h'
    #s.source_files = 'ZLKit_SwiftDemo/Classes/Bridging-Header.h'
#    s.private_header_files = 'ZLKit_SwiftDemo/Classes/Bridging_Header/Bridging_Header.h'
     
    s.subspec 'Extensions' do |ss|
        ss.source_files = 'ZLKit_SwiftDemo/Classes/Extensions/**/*'
        ss.frameworks = 'UIKit'
#        ss.resource_bundles = {
#            'Extensions' => ['ZLKit_SwiftDemo/Assets/Extensions/*']
#        }

        ss.subspec 'UIColor' do |sss|
            sss.source_files = 'ZLKit_SwiftDemo/Classes/Extensions/UIColor/**/*'
            sss.frameworks = 'UIKit'
        end

        ss.subspec 'BasicDataType' do |sss|
            sss.source_files = 'ZLKit_SwiftDemo/Classes/Extensions/BasicDataType/**/*'
            sss.frameworks = 'UIKit'
        end
        
        ss.subspec 'String' do |sss|
            sss.source_files = 'ZLKit_SwiftDemo/Classes/Extensions/String/**/*'
            sss.frameworks = 'UIKit'
        end
        
        ss.subspec 'Notifications' do |sss|
            sss.source_files = 'ZLKit_SwiftDemo/Classes/Extensions/Notifications/**/*'
            sss.frameworks = 'UIKit'
        end
        
        
    end
    
    s.subspec 'Defines' do |ss|
        ss.source_files = 'ZLKit_SwiftDemo/Classes/Defines/**/*'
        ss.frameworks = 'UIKit'
#        ss.resource_bundles = {
#            'Extensions' => ['ZLKit_SwiftDemo/Assets/Extensions/*']
#        }
    end
    
    
#    s.subspec 'Navigation' do |ss|
#        ss.frameworks = 'UIKit'
#        ss.source_files = 'ZLKit_SwiftDemo/Classes/Navigation/**/*'
#        ss.dependency 'SnapKit', '~> 4.2.0'
#    end
    
    s.subspec 'Fragments' do |ss|
        ss.frameworks = 'UIKit'
        ss.source_files = 'ZLKit_SwiftDemo/Classes/Fragments/**/*'
        ss.dependency 'ZLKit_Swift/Network'
        ss.dependency 'SnapKit', '~> 4.2.0'
        
        ss.subspec 'Imports' do |sss|
            sss.source_files = 'ZLKit_SwiftDemo/Classes/Fragments/Imports/**/*'
            sss.frameworks = 'UIKit'
            sss.dependency 'ZLKit_Swift/Extensions/Notifications'
            sss.dependency 'SnapKit', '~> 4.2.0'
        end
        
        ss.subspec 'Neumorphism' do |sss|
            sss.source_files = 'ZLKit_SwiftDemo/Classes/Fragments/Neumorphism/**/*'
            sss.frameworks = 'UIKit'
            sss.dependency 'ZLKit_Swift/Extensions/UIColor'
            sss.dependency 'SnapKit', '~> 4.2.0'
        end
        
    end
    
    s.subspec 'Network' do |ss|
        ss.frameworks = 'UIKit'
        ss.source_files = 'ZLKit_SwiftDemo/Classes/Network/**/*'
        ss.dependency 'ZLKit_Swift/Extensions'
        ss.dependency 'Alamofire', '~> 4.9.1'
#        ss.resource_bundles = {
#            'Extensions' => ['ZLKit_SwiftDemo/Assets/Network/*']
#        }
    end

    s.subspec 'AccreteDownload' do |ss|
        ss.frameworks = 'UIKit'
        ss.source_files = 'ZLKit_SwiftDemo/Classes/AccreteDownload/**/*'
        ss.dependency 'ZLKit_Swift/Extensions'
        ss.dependency 'Alamofire', '~> 4.9.1'
        ss.dependency 'SnapKit', '~> 4.2.0'
        #ss.dependency 'SDWebImage', '~> 5.12.5'
#        ss.resource_bundles = {
#            'Extensions' => ['ZLKit_SwiftDemo/Assets/Network/*']
#        }
    end
    
    s.swift_version = '5.0'
    s.frameworks = 'UIKit'
#    s.dependency 'ZLNavigationBar', '~> 4.1.3'

end
