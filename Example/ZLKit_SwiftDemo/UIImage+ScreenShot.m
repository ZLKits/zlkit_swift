//
//  UIImage+ScreenShot.m
//  BasicProduct
//
//  Created by 赵磊 on 2022/3/17.
//

#import "UIImage+ScreenShot.h"

@implementation UIImage (ScreenShot)

/// 截图比较清楚
+ (UIImage *)screenShot:(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(view.frame.size.width,view.frame.size.height ), NO, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];//renderInContext呈现接受者及其子范围到指定的上下文
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();//返回一个基于当前图形上下文的图片
    UIGraphicsEndImageContext();//移除栈顶的基于当前位图的图形上下文
    return viewImage;
}

// 裁剪图片
- (UIImage*)tailorSize:(CGRect)frame {
    CGImageRef originalImage_CG = self.CGImage;
    CGImageRef futureImageRef = CGImageCreateWithImageInRect(originalImage_CG, frame);
    CGSize size = CGSizeMake(frame.size.width, frame.size.height);
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawImage(context, frame, futureImageRef);
    UIImage *smallImage = [UIImage imageWithCGImage:futureImageRef];
    UIGraphicsEndImageContext();
    CGImageRelease(futureImageRef);
    return  smallImage;
}

@end
