//
//  ZLDownloaderImageLoaderView.swift
//  ZLKit_SwiftDemo_Example
//
//  Created by 赵磊 on 2022/8/13.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import ZLKit_Swift

class ZLDownloaderImageLoaderView: ZLDownloaderImageView {
    
    
    
    // 加载图片时的 SDWebImageOptions
    private var options: SDWebImageOptions = [.retryFailed, .decodeFirstFrameOnly, .allowInvalidSSLCertificates]
    

    override init(loadingSize: CGFloat = 35, progressWidth: CGFloat = 4, progressColor: UIColor = .red) {
        super.init(loadingSize: loadingSize, progressWidth: progressWidth, progressColor: progressColor)
        
        // 加载图片
        self.loadImageObserver = {[weak self] path, joinResults in
            guard let self = self else { return }
            self.sd_setImage(with: .init(fileURLWithPath: path), placeholderImage: nil, options: self.options) { image, _, _, _ in
                joinResults?(image)
            }
//            DispatchQueue.global().async {
//                let image = UIImage(contentsOfFile: path)
//                DispatchQueue.main.async {[weak self] in
//                    self?.image = image
//                    joinResults?(image)
//                }
//            }
        }
        
        // 配置缩略图的拼参方式
        self.priorityTryThumbnailUrlAppend = {
            // COS拼参方式 -- 控制在2M以内，不会超过2M
            "?imageMogr2/thumbnail/2097152@"
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
