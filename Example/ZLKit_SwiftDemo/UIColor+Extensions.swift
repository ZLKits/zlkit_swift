//
//  UIColor+Extensions.swift
//  ZLKit_SwiftDemo_Example
//
//  Created by 赵磊 on 2022/8/21.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation

extension UIColor {
    
    static let hex_F5FDFF: UIColor = .init(hexString: "#F5FDFF")
    static let hex_05A896: UIColor = .init(hexString: "#05A896")
    static let hex_95E6D3: UIColor = .init(hexString: "#95E6D3")
    
}
