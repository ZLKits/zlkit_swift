//
//  ZLViewController.swift
//  ZLFileDemo_Example
//
//  Created by iOS on 2022/8/4.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import UIKit
import ZLKit_Swift
import Alamofire

class ZLViewController: UIViewController {
    
    deinit {
//        downloaders.first?.fileDownload.cancelDownload()
        print("\(Date()): === 释放 ===, \(downloaders.first?.fileDownload)")
    }

    var downloaders: [AccreteDownloader] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .blue
        
//        FileCacheDefaults.resetFileCache(fileType: .image)
//        FileCacheDefaults.resetFileCache(fileType: .image, quality: .frame)
        
        let stk = UIStackView()
        stk.axis = .horizontal
        stk.spacing = 12
        view.addSubview(stk)
        stk.snp.makeConstraints({
            $0.top.equalToSuperview().inset(100)
            $0.centerX.equalToSuperview()
            $0.height.equalTo(100)
        })
        
        let btn = UIButton()
        btn.backgroundColor = .orange
        btn.setTitle("清理缓存", for: .normal)
        btn.addTarget(self, action: #selector(btnAction), for: .touchUpInside)
        stk.addArrangedSubview(btn)
        btn.snp.makeConstraints({
            $0.width.equalTo(100)
        })
        
        let suspendbtn = UIButton()
        suspendbtn.backgroundColor = .orange
        suspendbtn.setTitle("暂停", for: .normal)
        suspendbtn.addTarget(self, action: #selector(suspendbtnAction), for: .touchUpInside)
        stk.addArrangedSubview(suspendbtn)
        suspendbtn.snp.makeConstraints({
            $0.width.equalTo(100)
        })
        
        let imageView = ZLDownloaderImageLoaderView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .white
        view.addSubview(imageView)
        imageView.snp.makeConstraints({
            $0.size.equalTo(350)
//            $0.height.equalTo(150)
//            $0.top.equalTo(stk.snp.bottom).offset(30)
            $0.center.equalToSuperview()
        })
        
        let superTestbtn = UIButton()
        superTestbtn.backgroundColor = .orange
        superTestbtn.setTitle("超级测试", for: .normal)
        superTestbtn.addTarget(self, action: #selector(superTestbtnAction), for: .touchUpInside)
        view.addSubview(superTestbtn)
        superTestbtn.snp.makeConstraints({
            $0.top.equalTo(imageView.snp.bottom).offset(20)
            $0.centerX.equalToSuperview()
            $0.height.equalTo(100)
            $0.width.equalTo(200)
        })
        
        
        
//        requestImage(imageView: imageView, url: "https://pics1.baidu.com/feed/c75c10385343fbf250792e46ef52af8765388f36.jpeg?token=8b6c1e61e189c210825b0de504a9ea8b")
//        requestImage(imageView: imageView, url: "https://brand-res.artistnft.com.cn/yj/backstage/goodsImg/2251655457550_.pic_hd.jpg")
//        requestImage(imageView: imageView, url: "https://brand-res.artistnft.com.cn/yj/backstage/goodsImg/尾图.png")
//        requestImage(imageView: imageView, url: "https://brand-res-test.artistnft.com.cn/yj/backstage/goodsCoverImg/微信截图_20220622151029.png")
        
        
//        requestImage(imageView: imageView, url: "https://brand-res.artistnft.com.cn/yj/backstage/goodsImg/尾图.png")
//        requestImage(imageView: imageView, url: "https://brand-res.artistnft.com.cn/yj/backstage/goodsImg/尾图.png")
//        requestImage(imageView: imageView, url: "https://brand-res.artistnft.com.cn/yj/backstage/goodsImg/尾图.png")
//        requestImage(imageView: imageView, url: "https://brand-res.artistnft.com.cn/yj/backstage/goodsImg/尾图.png")
        
        view.layoutIfNeeded()
        let downloader = AccreteDownloader(url: requestImage())
        downloaders.append(downloader)
        
        downloader.getImageSize = { size in
            print("image.size = \(size)")
        }
        
        // 如果downloader没有imageView，则需要自己启动一下fire
//        downloader.fire()
        
//        imageView.downloader = downloader
    }
    
    @objc func btnAction() {
        FileCacheDefaults.resetFileCache(fileType: .image)
//        downloaders.first?.fileDownload.startDownloadFile()
    }
    
    @objc func suspendbtnAction() {
        
        downloaders.first?.fileDownload.suspendDownload()
    }
    
    @objc func superTestbtnAction() {
        navigationController?.pushViewController(ZLListTestTableViewController(), animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func requestImage() -> String {
        
        // 长图
//        let remoteUrl = "https://brand-test.infura-ipfs.io/ipfs/QmWvGWVcEdZn5jTXSaJNgY5t3C944s1qWvTAJSW8jkEuvW" // 22M 场景画 长图
//        let remoteUrl = "https://brand-res.artistnft.com.cn/yj/backstage/goodsImg/2251655457550_.pic_hd.jpg"// 6.2M 含文字的长图
        
//        let remoteUrl = "https://brand-test.infura-ipfs.io/ipfs/QmXb2cMsEkH7FdqFJiWq4SF4ASAjq4DsfLMEt73NXV9zqo" // 32M 花
        
//        let remoteUrl = "https://brand-res-test.artistnft.com.cn/yj/backstage/goodsCoverImg/微信截图_20220622151029.png" // 可爱风 - 粉色
//        let remoteUrl = "https://brand-test.infura-ipfs.io/ipfs/Qmf8Zox9KNsEkom5ZtfBTQU9SMuGjQTQZpsp9Va9X97Ef3" // 可爱风 - 粉色
        
        let remoteUrl = "https://pics1.baidu.com/feed/c75c10385343fbf250792e46ef52af8765388f36.jpeg?token=8b6c1e61e189c210825b0de504a9ea8b" //  56kb 美女图
        
//        let remoteUrl = "https://brand-res.artistnft.com.cn/yj/backstage/goodsImg/尾图.png" // 1.4M 文字
        
//        let remoteUrl = "https://brand-res-test.artistnft.com.cn/yj/backstage/goodsCoverImg/1518188244299710.gif" // gif图
        
        return remoteUrl
        
    }
    
    func requestVideo() {
        
    }
    
    

}

