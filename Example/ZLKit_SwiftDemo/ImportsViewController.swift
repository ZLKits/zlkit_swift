//
//  ImportsViewController.swift
//  ZLKit_SwiftDemo_Example
//
//  Created by 赵磊 on 2022/8/20.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import SnapKit
import ZLKit_Swift

class ImportsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .hex_FFFFFF
        title = "输入框的演示"
        
        testImport()
        
    }
    
    func testImport() {
        // 实例对象
        let textField = ZLTextField(type: .password)
        textField.placeholder = "输入内容"
        textField.backgroundColor = .lightGray
        textField.maxLength = 11
        view.addSubview(textField)
        textField.snp.makeConstraints({
            $0.top.equalToSuperview().inset(CGFloat.navHeight + 20)
            $0.leading.equalToSuperview().inset(20)
            $0.width.equalTo(200)
            $0.height.equalTo(50)
        })
        
        // 监听输入
        textField.editingChangedHandler = { text in
            print(text ?? "")
        }
        
        // 监听成为响应者
        textField.firstResponderHandler = { isResponder in
            print("isResponder = \(isResponder)")
        }
        
        // 监听安全性的切换
        textField.secureTextEntryHandler = { isSecure in
            print("isSecure = \(isSecure)")
        }
        
        // 设置安全性 -- 要在监听事件之后进行调整
        textField.isSecureTextEntry = false
    }
    
}
