//
//  UIImage+ScreenShot.h
//  BasicProduct
//
//  Created by 赵磊 on 2022/3/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (ScreenShot)

/// 截图比较清楚
+ (UIImage *)screenShot:(UIView *)view;

// 裁剪图片
- (UIImage*)tailorSize:(CGRect)frame;

@end

NS_ASSUME_NONNULL_END
