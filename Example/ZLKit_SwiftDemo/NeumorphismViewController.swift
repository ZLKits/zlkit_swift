//
//  NeumorphismViewController.swift
//  ZLKit_SwiftDemo_Example
//
//  Created by 赵磊 on 2022/8/21.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import ZLKit_Swift

class NeumorphismViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .hex_EBEBEB
        title = "新拟态的演示"
        
        testNeumorphism()
        
    }
    
    func testNeumorphism() {
        
        var lastView: ZLNeumorphismView
        
        // 内容承载
        var neumorphismView = ZLNeumorphismView(onlyBg: true)
        neumorphismView.ectadDoubleShadowView.backgroundColor = .hex_EBEBEB
        view.addSubview(neumorphismView)
        neumorphismView.snp.makeConstraints({
            $0.top.equalToSuperview().inset(CGFloat.navHeight + 20)
            $0.leading.equalToSuperview().inset(20)
            $0.size.equalTo(100)
        })
        
        // 反选
        lastView = neumorphismView
        neumorphismView = ZLNeumorphismView(onlyBg: true, type: .selected)
        neumorphismView.ectadDoubleShadowView.backgroundColor = .hex_EBEBEB
        neumorphismView.isSelected = false
        neumorphismView.entadDoubleShadowView.borderStyle = .center
        neumorphismView.entadDoubleShadowView.borderLayer.isHidden = false
        neumorphismView.entadDoubleShadowView.borderLayer.maskLayer.borderWidth = 2.5
        neumorphismView.entadDoubleShadowView.borderLayer.colors = [.hex_FFFFFF, .hex_95E6D3]
        neumorphismView.entadDoubleShadowView.lightColor = .hex_F5FDFF
        neumorphismView.entadDoubleShadowView.lightOpacity = 0.6
        neumorphismView.entadDoubleShadowView.darkColor = .hex_05A896
        neumorphismView.entadDoubleShadowView.darkOpacity = 1.0
        neumorphismView.entadDoubleShadowView.backgroundColor = .hex_95E6D3
        neumorphismView.cornerRadius = 10
        neumorphismView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectedItemAction(_:))))
        view.addSubview(neumorphismView)
        neumorphismView.snp.makeConstraints({
            $0.top.equalTo(lastView)
            $0.leading.equalTo(lastView.snp.trailing).offset(15)
            $0.size.equalTo(100)
        })
        
        // 禁用、启用
        lastView = neumorphismView
        neumorphismView = ZLNeumorphismView(type: .enabled)
        neumorphismView.cornerRadius = 28
        neumorphismView.titleLabel.text = "确定"
        neumorphismView.isEnabled = true
        neumorphismView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(enabledItemAction(_:))))
        view.addSubview(neumorphismView)
        neumorphismView.snp.makeConstraints({
            $0.top.equalTo(lastView.snp.bottom).offset(15)
            $0.leading.trailing.equalToSuperview().inset(20)
            $0.height.equalTo(56)
        })
        
        
    }
    
    // 反选的事件点击
    @objc func selectedItemAction(_ tap: UITapGestureRecognizer) {
        let view = tap.view as? ZLNeumorphismView ?? .init()
        view.isSelected.toggle()
    }
    
    // 禁用的事件点击
    @objc func enabledItemAction(_ tap: UITapGestureRecognizer) {
        let view = tap.view as? ZLNeumorphismView ?? .init()
        view.isEnabled.toggle()
    }
        
}
