//
//  ViewController.swift
//  ZLKit_SwiftDemo
//
//  Created by itzhaolei on 01/17/2022.
//  Copyright (c) 2022 itzhaolei. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    lazy var items: [[String: Any]] = {
        return Array<Any>.inMainBundle(forJsonName: "ViewControllerJson") as? [[String: Any]] ?? []
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "ZLKit使用手册"
        
    }
    
    @objc func btnAction() {
        navigationController?.pushViewController(ZLViewController(), animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    @available(iOS 2.0, *)
    func numberOfSections(in tableView: UITableView) -> Int {
        items.count
    }
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionItem = items[section]
        let rowItems = sectionItem["items"] as? [[String: String]]
        return rowItems?.count ?? 0
    }
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = NSStringFromClass(UITableViewCell.classForCoder())
        var cell = tableView.dequeueReusableCell(withIdentifier: identifier)
        if cell == nil {
            cell = .init(style: .subtitle, reuseIdentifier: identifier)
            cell?.textLabel?.textColor = .hex_333333
            cell?.detailTextLabel?.textColor = .hex_555555
            cell?.textLabel?.snp.makeConstraints({
                $0.top.equalToSuperview().inset(10)
                $0.leading.equalToSuperview().inset(50)
                $0.trailing.equalToSuperview().inset(26)
            })
            let textLabel = cell?.textLabel ?? .init()
            cell?.detailTextLabel?.snp.makeConstraints({
                $0.top.equalTo(textLabel.snp.bottom).offset(10)
                $0.leading.equalToSuperview().inset(50)
                $0.trailing.equalToSuperview().inset(26)
                $0.bottom.equalToSuperview().inset(10)
            })
        }
        guard let cell = cell else { return .init() }
        let sectionItem = items[indexPath.section]
        let rowItems = sectionItem["items"] as? [[String: String]]
        let rowItem = rowItems?[indexPath.row]
        cell.textLabel?.text = rowItem?["title"]
        cell.detailTextLabel?.text = rowItem?["details"]
        let id = Int(rowItem?["id"] ?? "") ?? 0
        cell.accessoryType = id > 0 ? .disclosureIndicator : .none
        return cell
    }
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        50
    }

    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        20
    }

    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .hex_FFFFFF
        let label = UILabel()
        label.font = .pingFangSC(ofSize: 12, weight: .bold)
        label.textColor = .hex_111111
        view.addSubview(label)
        label.snp.makeConstraints({
            $0.leading.trailing.equalToSuperview().inset(26)
            $0.top.bottom.equalToSuperview()
        })
        // 赋值
        let sectionItem = items[section]
        label.text = sectionItem["title"] as? String ?? ""
        return view
    }

    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        .init()
    }
    
    @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sectionItem = items[indexPath.section]
        let rowItems = sectionItem["items"] as? [[String: String]]
        let rowItem = rowItems?[indexPath.row]
        let id = rowItem?["id"]
        if id == "1" { // 输入框的演示
            navigationController?.pushViewController(ImportsViewController(), animated: true)
            return
        }
        if id == "2" { // 新拟态的演示
            navigationController?.pushViewController(NeumorphismViewController(), animated: true)
            return
        }
    }
    
}
