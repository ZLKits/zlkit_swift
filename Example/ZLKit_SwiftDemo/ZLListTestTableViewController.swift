//
//  ZLListTestTableViewController.swift
//  ZLKit_SwiftDemo_Example
//
//  Created by 赵磊 on 2022/8/26.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import UIKit
import ZLKit_Swift

class ZLListTestTableViewController: UIViewController {
    
    var models: [AccreteDownloader] = []
    
    lazy var collectionView: UITableView = {
        let view = UITableView()
        view.clear()
        view.dataSource = self
        view.register(ZLListTestTableViewCell.classForCoder(), forCellReuseIdentifier: NSStringFromClass(ZLListTestTableViewCell.classForCoder()))
        
//        //刷新
//        let normalHeader = MJRefreshStateHeader.init {[weak self] in
//            guard let self = self else { return }
//            self.parseData(isLoadMore: false)
//        }
//        view.mj_header = normalHeader;
//
//        //加载更多
//        let footer = MJRefreshAutoStateFooter.init(refreshingBlock: {[weak self] in
//            guard let self = self else { return }
//            self.parseData(isLoadMore: true)
//        })
//        view.mj_footer = footer
        
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .lightGray
        
        // 先清空，每次进来都要重新加载网络的，以达到测试目的
        FileCacheDefaults.resetFileCache(fileType: .image)
                
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints({
            $0.edges.equalToSuperview()
        })
        
        parseData(isLoadMore: false)
        collectionView.reloadData()
    }
    
    func parseData(isLoadMore: Bool) {
        
        var urls: [String] = []
        
        var remoteUrls:[String] = [
            "https://brand-res.artistnft.com.cn/ipfs/QmV9Ajd8HhXK5qmwaEmQn1cmWife7pAPDFzoWqAa968cFF",
            "https://brand-res.artistnft.com.cn/ipfs/QmV9Ajd8HhXK5qmwaEmQn1cmWife7pAPDFzoWqAa968cFF",
            "https://brand-res.artistnft.com.cn/ipfs/QmV9Ajd8HhXK5qmwaEmQn1cmWife7pAPDFzoWqAa968cFF",
            "https://brand-res.artistnft.com.cn/ipfs/QmSacP9DsWJXMMUMUQ4brYoNvBuhypJJYSQEHA5i5xDfS5",
            "https://brand-res.artistnft.com.cn/ipfs/QmVUN3tFjCxty7tfqRQzg26zf1kLQp27zfEG4Zsn4S66xX",
            "https://brand-res.artistnft.com.cn/ipfs/QmVUN3tFjCxty7tfqRQzg26zf1kLQp27zfEG4Zsn4S66xX",
            "https://brand-res.artistnft.com.cn/ipfs/QmevnXLFQUj6tUvd8FL5UPfrWC5r1VmfL6CePaxLDZhY1d",
            "https://brand-res.artistnft.com.cn/ipfs/Qmc5E6DKJohjURVwYxFyWqs3Go9CLY2xPDZDnmydv8WgPG",
            "https://brand-res.artistnft.com.cn/ipfs/QmUG4vgyAHkhvyzK7vtXvvW8Rzps9QwJZAhj6o7c9HLQKS",
            "https://brand-res.artistnft.com.cn/ipfs/QmSKFBs1y261979EU63ZsU76Cj1VSXznFi9x7xawHUxBS3",
            "https://brand-res.artistnft.com.cn/ipfs/QmUCRotWuQsdBS5LXExUtKZykfAPtSrRocPUiCVLmtWjd5",
            "https://brand-res.artistnft.com.cn/ipfs/QmcpTJFTq4UJmBZADThyUiwwRfb3rAzKo5Wgr5Fex4Ze8D",
            "https://brand-res.artistnft.com.cn/ipfs/QmdrH5UkkBp8E3fUvxvZ8AzicmSJMGjdpvDQnHbZMu6Axr",
            "https://brand-res.artistnft.com.cn/ipfs/QmVUN3tFjCxty7tfqRQzg26zf1kLQp27zfEG4Zsn4S66xX",
            "https://brand-res.artistnft.com.cn/ipfs/QmcsXSkr5gfzgZeFyPEddaQ9ACsE2UEaxsdC15VtYg9dZX",
            "https://brand-res.artistnft.com.cn/ipfs/QmcsXSkr5gfzgZeFyPEddaQ9ACsE2UEaxsdC15VtYg9dZX",
            "https://brand-res.artistnft.com.cn/ipfs/QmcsXSkr5gfzgZeFyPEddaQ9ACsE2UEaxsdC15VtYg9dZX",
            "https://brand-res.artistnft.com.cn/ipfs/QmcsXSkr5gfzgZeFyPEddaQ9ACsE2UEaxsdC15VtYg9dZX",
            "https://brand-res.artistnft.com.cn/ipfs/QmWmrSk7Ctha92rh75wvDx6UgU8nchEtgzAC1aKbfZkDA3",
            "https://brand-res.artistnft.com.cn/ipfs/QmV9Ajd8HhXK5qmwaEmQn1cmWife7pAPDFzoWqAa968cFF",
            "https://brand-res.artistnft.com.cn/ipfs/QmV9Ajd8HhXK5qmwaEmQn1cmWife7pAPDFzoWqAa968cFF",
            "https://brand-res.artistnft.com.cn/ipfs/QmV9Ajd8HhXK5qmwaEmQn1cmWife7pAPDFzoWqAa968cFF",
            "https://brand-res.artistnft.com.cn/ipfs/QmZ7nL8vPyBGDJ1dybBqbcErcPqco1cPMQq84zZXM2DxTu",
//            "https://brand-res.artistnft.com.cn/ipfs/QmV9Ajd8HhXK5qmwaEmQn1cmWife7pAPDFzoWqAa968cFF",
//            "https://brand-res.artistnft.com.cn/ipfs/QmUSJukGrby66ZoWUVqEePFKTrToVQtnAfajeioG2Lgz6y",
//            "https://brand-res.artistnft.com.cn/ipfs/QmbbccxHadF5xoFubF98ovdMCKTMXNsgQQe1yT8iBjVAj8",
//            "https://brand-res.artistnft.com.cn/ipfs/QmP183VKLSqNf6nXsKaaYPdxj8gDGb9x3b3TDAGeDC6rw1",
//            "https://brand-res.artistnft.com.cn/ipfs/QmP6F1bKUx5RrA9tji746gkdNsWZcf1AB69mBANRrcQNXX",
//            "https://brand-res.artistnft.com.cn/ipfs/QmRefSB6h39vN8XfwtuF182ELCAikRxWaBukYAuiiZMSK1",
//            "https://brand-res.artistnft.com.cn/ipfs/QmStKosqAWcCShc2RY4UFF2QCvtVT5FvmqqmrwSjPAHzHz",
//            "https://brand-res.artistnft.com.cn/ipfs/QmTP4Xoi4jmkpokPaGDmaac2p2nEMw9B3bdASQxEYypkJ2",
//            "https://brand-res.artistnft.com.cn/ipfs/QmTYTJKCnUQDJpZsziyUoag1qw3PreprNMMtDXKnrLz3uT",
//            "https://brand-res.artistnft.com.cn/ipfs/QmV4dK8PXoyk1fAQUTyq6rgue6FMAL4ih1qRmex47X4RSq",
//            "https://brand-res.artistnft.com.cn/ipfs/QmVc42hVqShBZ7RXCRUnn5TgrAMLdHHKc2zUk8JXd7e12V",
//            "https://brand-res.artistnft.com.cn/ipfs/QmXAifFJsnTv2n1hHGDShkeqLE4a1bYUdN4v8adRvwifKt",
//            "https://brand-res.artistnft.com.cn/ipfs/QmY1jJPnHTRLMJg7Vv3rPsp4v6hSgcVXedW8MK7fSTFwJV",
//            "https://brand-res.artistnft.com.cn/ipfs/QmYitJgXRjiaGobi25TxQyocnay6ajXBNsb72XiwqvjYa3",
//            "https://brand-res.artistnft.com.cn/ipfs/QmZRuKwk85HefNUrc5Au13JZNVVuGnXNCVT7axemASKLzu",
//            "https://brand-res.artistnft.com.cn/ipfs/QmS8rm8tj2GvpK66Ss9JjjoSFznttKF9HPLTTs6Y9744KE",
//            "https://brand-res.artistnft.com.cn/ipfs/QmZRuKwk85HefNUrc5Au13JZNVVuGnXNCVT7axemASKLzu",
//            "http://brand-1313227452.cos.ap-guangzhou.myqcloud.com/yj/backstage/goodsCoverImg/16607184197485357.png",
//            "https://brand-res.artistnft.com.cn/yj/backstage/goodsCoverImg/WechatIMG495.jpeg",
//            "https://brand-res.artistnft.com.cn/yj/backstage/goodsCoverImg/封面图3.jpg",
////            "https://i.ibb.co/T2nX0Bh/xxx.jpg",
//            "https://brand-res.artistnft.com.cn/yj/backstage/goodsCoverImg/封面图2.jpg",
//            "https://brand-res.artistnft.com.cn/yj/backstage/goodsCoverImg/封面图1.jpg",
//            "https://brand-res.artistnft.com.cn/yj/backstage/goodsCoverImg/肖烟99元666个.jpg",
//            "https://brand-res.artistnft.com.cn/yj/backstage/goodsCoverImg/耿毅99元666个.jpg",
//            "https://brand-res.artistnft.com.cn/yj/backstage/goodsCoverImg/胡卫国99元666个.jpg",
//            "https://brand-res.artistnft.com.cn/yj/backstage/goodsCoverImg/新年卡片166元333个.jpg",
//            "https://brand-res.artistnft.com.cn/yj/backstage/goodsCoverImg/畅通无阻.jpg",
        ]
        
        
        for index in 0..<remoteUrls.count {
            let url = remoteUrls[index]
//            urls.append("https://brand-test.infura-ipfs.io/ipfs/QmWvGWVcEdZn5jTXSaJNgY5t3C944s1qWvTAJSW8jkEuvW") // 22M
//            urls.append("https://brand-res.artistnft.com.cn/yj/backstage/goodsImg/2251655457550_.pic_hd.jpg") // 6.2M
//            urls.append("https://brand-res.artistnft.com.cn/yj/backstage/goodsImg/尾图.png") // 1.2M
//            urls.append("https://brand-res.artistnft.com.cn/ipfs/QmZRuKwk85HefNUrc5Au13JZNVVuGnXNCVT7axemASKLzu")
            urls.append(url)
        }
        
        if !isLoadMore {
            models = []
        }
        
        for index in 0..<urls.count {
            let url = urls[index]
            let downloader = AccreteDownloader(url: url)
            models.append(downloader)
        }
        
        collectionView.reloadData()
        collectionView.mj_header?.endRefreshing()
        collectionView.mj_footer?.endRefreshing()
        
    }
        
}

extension ZLListTestTableViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = collectionView.dequeueReusableCell(withIdentifier: NSStringFromClass(ZLListTestTableViewCell.classForCoder()), for: indexPath) as? ZLListTestTableViewCell ?? .init()
        cell.coverImageView.indexPath = indexPath
        cell.coverImageView.downloader = models[indexPath.row]
        cell.titleLabel.text = "\(indexPath.row)"
//        print("加载 = \(indexPath)")
        return cell
    }
    
}

class ZLListTestTableViewCell: UITableViewCell {
    
    lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.backgroundColor = .hex_111111
        view.font = .systemFont(ofSize: 12)
        view.textColor = .hex_FFFFFF
        return view
    }()
    
    lazy var coverImageView: ZLDownloaderImageLoaderView = {
        let view = ZLDownloaderImageLoaderView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.backgroundColor = .white
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(coverImageView)
        let scale: CGFloat = 256.0 / 154.0
        let width = (UIScreen.main.bounds.width - 26.0 * 2 - 15.0) / 2
        coverImageView.snp.makeConstraints({
            $0.edges.equalToSuperview()
            $0.width.equalTo(width)
            $0.height.equalTo(width * scale)
        })
        contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints({
            $0.trailing.top.equalToSuperview()
            $0.width.equalTo(100)
            $0.height.equalTo(20)
        })
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
