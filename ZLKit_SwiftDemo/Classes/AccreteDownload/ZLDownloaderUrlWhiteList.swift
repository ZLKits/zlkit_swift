//
//  ZLDownloaderUrlWhiteList.swift
//  ZLKit_Swift
//
//  Created by 赵磊 on 2022/8/16.
//

import Foundation

class ZLDownloaderUrlWhiteList: NSObject {
    
    static let shared: ZLDownloaderUrlWhiteList = .init()
    
    var whiteList: [String] = []
    
}
