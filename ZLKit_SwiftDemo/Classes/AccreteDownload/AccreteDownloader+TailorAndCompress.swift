//
//  ZLDownloaderImageView+TailorAndCompress.swift
//  ZLKit_Swift
//
//  Created by iOS on 2022/8/5.
//

import Foundation

extension AccreteDownloader {
    
    /* 这段代码设计之初是想通过控件大小来生成对应的缩略缓存，下次相同的大小就直接获取对应尺寸的缓存，以此来节省内存。但是，处于列表时，裁剪图片会造成内存的瞬间飙升，导致闪退。所以就去除了裁剪功能，只进行降低图片质量，来节省内存。
     */
    
    /// 裁剪和压缩图片
    /// - Parameters:
    ///   - data: 二进制
    ///   - lowAccuracy: 低精度的
    func tailorAndCompress(data: Data, lowAccuracy: Bool, result: Params_one_void<Any?>) {
        DispatchQueue.global().async {
            
            // 超过 1M 再考虑进行压缩，不超过就不压缩
            let onMinDataLength: Int = 1024 * 1024 * 2
            guard data.count > onMinDataLength else {
                // 不超过
                DispatchQueue.main.async {
                    result?(data)
                }
                return
            }
            
            // 超过、进行压缩
            var data: Data = data
            let image = UIImage(data: data) ?? .init()

            let dataLength = Double(data.count) / 1024.0 / 1024
            var toSize: Double = 0
            if dataLength > 5.0 {
                toSize = 2 * 1024.0
            }else {
                toSize = 1.3 * 1024.0
            }
            var compressionScale =  toSize / Double(data.count)
            if !lowAccuracy {
                // 高精度
                if image.size.height / image.size.width > 2 {
                    // 长图，就给高点精度，避免模糊
                    compressionScale = 0.55
                }else {
                    compressionScale = 0.2
                }
            }
            data = image.jpegData(compressionQuality: compressionScale) ?? .init()

            DispatchQueue.main.async {
                result?(data)
            }
        }
    }
    
}

