//
//  ShadowView.m
//  ZLBannerDemo
//
//  Created by iOS on 2022/6/10.
// 更新一下

#import "ZLEntadShadowLayer.h"

@interface ZLEntadShadowLayer ()

@property (nonatomic, weak) CAShapeLayer *lastLayer;

@end

@implementation ZLEntadShadowLayer

/// 月食效果
- (void)updateShadow:(CGPoint)offset Color:(UIColor *)color Opacity:(CGFloat)opacity Diameter:(CGFloat)diameter CornerRadius:(CGFloat)cornerRadius {
    // 先移除之前的
    [self.lastLayer removeFromSuperlayer];
    self.lastLayer = nil;
    // 添加新的
    CAShapeLayer* shadowLayer = [CAShapeLayer layer];
    shadowLayer.fillColor = color.CGColor;
    [shadowLayer setFrame:self.bounds];
    [shadowLayer setShadowColor:color.CGColor];
    [shadowLayer setShadowOffset:CGSizeMake(offset.x, offset.y)];
    [shadowLayer setShadowOpacity:opacity];
    [shadowLayer setFillRule:kCAFillRuleEvenOdd];
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path,NULL,CGRectInset(self.bounds, -diameter, -diameter));
    CGPathRef someInnerPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:cornerRadius].CGPath;
    CGPathAddPath(path,NULL, someInnerPath);
    CGPathCloseSubpath(path);
    [shadowLayer setPath:path];
    CGPathRelease(path);
    [self addSublayer:shadowLayer];
    self.lastLayer = shadowLayer;
    // 蒙版
    CAShapeLayer* maskLayer = [CAShapeLayer layer];
    [maskLayer setPath:someInnerPath];
    [shadowLayer setMask:maskLayer];
    
}

@end
