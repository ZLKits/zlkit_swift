//
//  EntadDoubleShadowView.swift
//  BasicProduct
//
//  Created by iOS on 2022/6/9.
//

import UIKit

public enum ZLBorderStyle {
    case outside
    case inside
    case center
}

/// 向内双阴影
open class ZLEntadDoubleShadowView: UIView {
        
    public var borderStyle: ZLBorderStyle = .inside {
        didSet {
            if borderStyle == .outside {
                borderLayer.cornerRadius = cornerRadius + borderLayer.maskLayer.borderWidth
            }else if borderStyle == .center {
                borderLayer.cornerRadius = cornerRadius + borderLayer.maskLayer.borderWidth / 2
            }
            layoutSubviews()
        }
    }
    
    public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            if borderStyle == .outside {
                borderLayer.cornerRadius = cornerRadius + borderLayer.maskLayer.borderWidth
            }else if borderStyle == .center {
                borderLayer.cornerRadius = cornerRadius + borderLayer.maskLayer.borderWidth / 2
            }
            layoutSubviews()
        }
    }
    
    public var lightOpacity: CGFloat = 1.0
    public var lightColor: UIColor = .hex_FFFFFF
    public var lightOffset: CGPoint = .zero
    public var lightDiameter: CGFloat = 10
    
    public var darkOpacity: CGFloat = 1.0
    public var darkColor: UIColor = .hex_364883
    public var darkOffset: CGPoint = .zero
    public var darkDiameter: CGFloat = 10
    
    
    lazy public var lightLayer: ZLEntadShadowLayer = {
        let layer = ZLEntadShadowLayer()
        return layer
    }()
    
    lazy public var darkLayer: ZLEntadShadowLayer = {
        let view = ZLEntadShadowLayer()
        return view
    }()
    
    public lazy var borderLayer: ZLEctadDoubleShadowView.BorderLayer = {
        let view = ZLEctadDoubleShadowView.BorderLayer()
        view.defaultConfig()
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        lightLayer.frame = bounds
        lightLayer.updateShadow(lightOffset, color: lightColor, opacity: lightOpacity, diameter: lightDiameter, cornerRadius: cornerRadius)
        darkLayer.frame = bounds
        darkLayer.updateShadow(darkOffset, color: darkColor, opacity: darkOpacity, diameter: darkDiameter, cornerRadius: cornerRadius)
        var borderX: CGFloat = 0
        var borderY: CGFloat = 0
        var borderWidth: CGFloat = bounds.width
        var borderHeight: CGFloat = bounds.height
        if borderStyle == .center {
            borderX = -borderLayer.maskLayer.borderWidth / 2
            borderY = -borderLayer.maskLayer.borderWidth / 2
            borderWidth = bounds.width + borderLayer.maskLayer.borderWidth
            borderHeight = bounds.height + borderLayer.maskLayer.borderWidth
        }else if borderStyle == .outside {
            borderX = -borderLayer.maskLayer.borderWidth
            borderY = -borderLayer.maskLayer.borderWidth
            borderWidth = bounds.width + borderLayer.maskLayer.borderWidth * 2
            borderHeight = bounds.height + borderLayer.maskLayer.borderWidth * 2
        }
        borderLayer.frame = .init(x: borderX, y: borderY, width: borderWidth, height: borderHeight)
    }
    
    private func addSubviews() {
        layer.addSublayer(darkLayer)
        layer.addSublayer(lightLayer)
        layer.addSublayer(borderLayer)
    }
    
}

// 事件区
extension ZLEntadDoubleShadowView {
    
    /// 默认配置
    ///
    ///```
    ///backgroundColor = .hex_EBEBEB
    ///cornerRadius = 10
    ///lightOpacity = 0.9
    ///lightColor = .hex_FFFFFF
    ///lightOffset = .init(x: -2, y: -3)
    ///lightDiameter = 5
    ///darkOpacity = 0.5
    ///darkColor = .hex_888888
    ///darkOffset = .init(x: 2, y: 3)
    ///darkDiameter = 5
    ///borderLayer.maskLayer.borderWidth = 1.5
    ///borderLayer.colors = [.hex_FFFFFF, .hex_D1D1D1]
    ///borderStyle = .outside
    ///```
    public func defaultConfig() {
        backgroundColor = .hex_EBEBEB
        cornerRadius = 10
        lightOpacity = 0.9
        lightColor = .hex_FFFFFF
        lightOffset = .init(x: -2, y: -3)
        lightDiameter = 5
        darkOpacity = 0.5
        darkColor = .hex_888888
        darkOffset = .init(x: 2, y: 3)
        darkDiameter = 5
        borderLayer.maskLayer.borderWidth = 1.5
        borderLayer.colors = [.hex_FFFFFF, .hex_D1D1D1]
        borderStyle = .inside
    }
    
}

// 定制区
extension ZLEntadDoubleShadowView {
    
    
    
}
