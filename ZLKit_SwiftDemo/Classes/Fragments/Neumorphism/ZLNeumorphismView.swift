//
//  NextItemBgView_V2.swift
//  BasicProduct
//
//  Created by iOS on 2022/7/19.
//

import UIKit

open class ZLNeumorphismView: UIView {
    
    public enum ItemType: Int {
        /// 单视图
        case content
        /// 启用、禁用
        case enabled
        /// 选中、非选中
        case selected
    }
    
    /// 选中反选的事件同步
    public var selectedHandler: Params_one_void<Bool> = nil
    
    /// 启用禁用的事件同步
    public var enabledHandler: Params_one_void<Bool> = nil
    
    public var isSelected: Bool = false {
        didSet {
            guard type == .selected else { return }
            if isColourful {
                colourfulEctadDoubleShadowView.isHidden = isSelected
            }else {
                ectadDoubleShadowView.isHidden = isSelected
            }
            entadDoubleShadowView.isHidden = !isSelected
            if !onlyBg {
                if isColourful {
                    titleLabel.textColor = isSelected ? .hex_111111 : .hex_555555
                }else {
                    titleLabel.textColor = isSelected ? .hex_EBEBEB : .hex_555555
                }
            }
            selectedHandler?(isSelected)
        }
    }
    
    public var isEnabled: Bool = false {
        didSet {
            guard type == .enabled else { return }
            if isColourful {
                colourfulEctadDoubleShadowView.isHidden = !isEnabled
            }else {
                ectadDoubleShadowView.isHidden = !isEnabled
            }
            entadDoubleShadowView.isHidden = isEnabled
            isUserInteractionEnabled = isEnabled
            if !onlyBg {
                if isColourful {
                    titleLabel.textColor = isEnabled ? .hex_111111 : .hex_555555
                }else {
                    titleLabel.textColor = isEnabled ? .hex_EBEBEB : .hex_555555
                }
            }
            enabledHandler?(isEnabled)
        }
    }
    
    public var cornerRadius: CGFloat = 0 {
        didSet {
            if isColourful {
                colourfulEctadDoubleShadowView.layer.cornerRadius = cornerRadius
            }else {
                ectadDoubleShadowView.cornerRadius = cornerRadius
            }
            entadDoubleShadowView.cornerRadius = cornerRadius
        }
    }
    
    /// 纯黑色可点击状态
    public lazy var ectadDoubleShadowView: ZLEctadDoubleShadowView = {
        let view = ZLEctadDoubleShadowView()
        view.defaultConfig()
        view.backgroundColor = .hex_111111
        view.lightOffset = .init(x: -2, y: -3)
        view.lightDiameter = 6
        view.darkDiameter = 8
        return view
    }()
    
    /// 彩色可点击状态
    public lazy var colourfulEctadDoubleShadowView: ZLGradualView = {
        let view = ZLGradualView()
        view.colors = [.hex_94C6FF, .hex_6CF5C5]
        view.startPoint = .leftTop
        view.endPoint = .rightTop
        view.layer.masksToBounds = true
        return view
    }()
    
    /// 向内阴影
    public lazy var entadDoubleShadowView: ZLEntadDoubleShadowView = {
        let view = ZLEntadDoubleShadowView()
        view.defaultConfig()
        view.borderLayer.isHidden = true
        view.backgroundColor = .hex_DDDDDD
        view.lightOffset = .init(x: -3, y: -3)
        view.lightDiameter = 6
        view.lightColor = .hex_FFFFFF
        view.darkOffset = .init(x: 2, y: 3)
        view.darkDiameter = 10
        view.darkColor = .hex_888888
        view.darkOpacity = 1.0
        return view
    }()
    
    public lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.font = .pingFangSC(ofSize: 18, weight: .bold)
        view.textAlignment = .center
        return view
    }()
    
    private let onlyBg: Bool
    private let isColourful: Bool
    private let type: ItemType
    private let textInset: UIEdgeInsets?
    public init(onlyBg: Bool = false, type: ItemType = .content, isColourful: Bool = false, textInset: UIEdgeInsets? = nil) {
        self.onlyBg = onlyBg
        self.type = type
        self.isColourful = isColourful
        self.textInset = textInset
        super.init(frame: .zero)
        clipsToBounds = false
        addSubviews()
        addConstraints()
        addActions()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        if type == .content {
            addSubview(ectadDoubleShadowView)
        }else {
            if isColourful {
                addSubview(colourfulEctadDoubleShadowView)
            }else {
                addSubview(ectadDoubleShadowView)
            }
            addSubview(entadDoubleShadowView)
        }
        if !onlyBg {
            addSubview(titleLabel)
        }
    }
    
    private func addConstraints() {
        if type == .content {
            ectadDoubleShadowView.snp.makeConstraints({
                $0.edges.equalToSuperview()
            })
        }else {
            if isColourful {
                colourfulEctadDoubleShadowView.snp.makeConstraints({
                    $0.edges.equalToSuperview()
                })
            }else {
                ectadDoubleShadowView.snp.makeConstraints({
                    $0.edges.equalToSuperview()
                })
            }
            entadDoubleShadowView.snp.makeConstraints({
                $0.edges.equalToSuperview()
            })
        }
        if !onlyBg {
            titleLabel.snp.makeConstraints({
                $0.edges.equalToSuperview().inset(textInset ?? .zero)
            })
        }
    }
    
    private func addActions() {
        
    }
    
}

// 事件区
extension ZLNeumorphismView {
    
    
    
}

// 定制区
extension ZLNeumorphismView {
    
    
    
}
