//
//  GradualBorderLayer.swift
//  BasicProduct
//
//  Created by iOS on 2022/6/24.
//

import UIKit

class ZLGradualBorderView: UIView {
    
    public enum BorderStyle {
        case outside
        case inside
        case center
    }
    
    public var borderStyle: BorderStyle = .inside
    
    public var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = true
            borderLayer.cornerRadius = cornerRadius
        }
    }
    
    public lazy var borderLayer: ZLEctadDoubleShadowView.BorderLayer = {
        let view = ZLEctadDoubleShadowView.BorderLayer()
        view.defaultConfig()
        view.colors = [UIColor.hex_FFFFFF.withAlphaComponent(0.347), .hex_A8A8A8]
        return view
    }()
            
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
        addConstraints()
        addActions()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        var borderX: CGFloat = 0
        var borderY: CGFloat = 0
        var borderWidth: CGFloat = bounds.width
        var borderHeight: CGFloat = bounds.height
        if borderStyle == .center {
            borderX = -borderLayer.maskLayer.borderWidth / 2
            borderY = -borderLayer.maskLayer.borderWidth / 2
            borderWidth = bounds.width + borderLayer.maskLayer.borderWidth
            borderHeight = bounds.height + borderLayer.maskLayer.borderWidth
        }else if borderStyle == .outside {
            borderX = -borderLayer.maskLayer.borderWidth
            borderY = -borderLayer.maskLayer.borderWidth
            borderWidth = bounds.width + borderLayer.maskLayer.borderWidth * 2
            borderHeight = bounds.height + borderLayer.maskLayer.borderWidth * 2
        }
        borderLayer.frame = .init(x: borderX, y: borderY, width: borderWidth, height: borderHeight)
        CATransaction.commit()
    }
    
    private func addSubviews() {
        layer.addSublayer(borderLayer)
    }
    
    private func addConstraints() {
        
    }
    
    private func addActions() {
        
    }
    
}

// 事件区
extension ZLGradualBorderView {
    
    
    
}

// 定制区
extension ZLGradualBorderView {
    
    
    
}
