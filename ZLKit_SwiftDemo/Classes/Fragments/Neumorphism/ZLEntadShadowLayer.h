//
//  ShadowView.h
//  ZLBannerDemo
//
//  Created by iOS on 2022/6/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZLEntadShadowLayer : CAShapeLayer

/// 月食效果
- (void)updateShadow:(CGPoint)offset Color:(UIColor *)color Opacity:(CGFloat)opacity Diameter:(CGFloat)diameter CornerRadius:(CGFloat)cornerRadius;

@end

NS_ASSUME_NONNULL_END
