//
//  CGFloat+ZLExtensions.swift
//  ZLKit_SwiftDemo_Example
//
//  Created by itzhaolei on 2022/1/17.
//  Copyright © 2022 CocoaPods. All rights reserved.
//


// 无参
public typealias Basic_void = (() -> Void)?
public typealias Basic_return<Return> = (() -> Return)?

// 单参
public typealias Params_one_void<Param> = ((Param) -> Void)?
public typealias Params_one_return<Param, Return> = ((Param) -> Return)?

// 双参
public typealias Params_two_void<Param1, Param2> = ((Param1, Param2) -> Void)?
public typealias Params_tow_return<Param1, Param2, Return> = ((Param1, Param2) -> Return)?

// 三参
public typealias Params_three_void<Param1, Param2, Param3> = ((Param1, Param2, Param3) -> Void)?
public typealias Params_three_return<Param1, Param2, Param3, Return> = ((Param1, Param2, Param3) -> Return)?

// 四参
public typealias Params_four_void<Param1, Param2, Param3, Param4> = ((Param1, Param2, Param3, Param4) -> Void)?
public typealias Params_four_return<Param1, Param2, Param3, Param4, Return> = ((Param1, Param2, Param3, Param4) -> Return)?

// 五参
public typealias Params_five_void<Param1, Param2, Param3, Param4, Param5> = ((Param1, Param2, Param3, Param4, Param5) -> Void)?
public typealias Params_five_return<Param1, Param2, Param3, Param4, Param5, Return> = ((Param1, Param2, Param3, Param4, Param5) -> Return)?
