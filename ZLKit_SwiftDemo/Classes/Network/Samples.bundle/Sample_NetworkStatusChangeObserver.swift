//
//  Sample_NetworkStatusChangeObserver.swift
//  ZLKit_Swift
//
//  Created by itzhaolei on 2022/1/18.
//


// 示例：演示网络状态变化的观察
class Sample_NetworkStatusChangeObserver: NSObject {
    
    override init() {
        super.init()
        
        // 进行监听
        addNotification(name: .deviceNetworkChanged, observer: self, selector: #selector(deviceNetworkChangedAction(_:)))
        
    }
    
    // 处理跟随事件
    @objc func deviceNetworkChangedAction(_ notifi: Notification) {
        print(notifi)
        switch notifi.userInfo?[NotificationUserInfoNetworkStatusKey] as? NetworkStatus {
            case .reachable(.ethernetOrWiFi): // WIFI
                
                break
            case .reachable(.wwan): // 蜂窝网络
                
                break
            default: // 无网络 或 未询问
            
                break
        }
    }
    
}
