//
//  SampleAPI.swift
//  ZLKit_Swift
//
//  Created by itzhaolei on 2022/1/18.
//

import Alamofire

// 示例
enum SampleAPI: URLRequestConvertible {
    
    case sendAuthCode(phone: String)
    case login(phone: String, authCode: String)
    
    func asURLRequest() -> URLRequest {
        var url: URL = {
            let relativePath: String?
            switch self {
            case .sendAuthCode:
                relativePath = "/xx/xx/sendAuthCode"
            case .login:
                relativePath = "/xx/xx/login"
            }
            return apiDomainAppending(path: relativePath)
        }()
        
        var method: HTTPMethod {
            switch self {
            case .sendAuthCode:
                return .get
            case .login:
                return .post
            }
        }
        
        let params: ([String: Any]?) = {
            switch self {
            case .sendAuthCode(let phone):
                let params = "?phone=\(phone)"
                url = appending(url: url, params: params)
                return nil
            case .login(let phone, let authCode):
                return ["phone": phone, "authCode": authCode]
            }
        }()
        
        return request(url: url, method: method, allHTTPHeaderFields: [:], params: params)
    }
}

// 示例
public extension NetworkService {
    
    /// 示例：发送验证码
    func sample_sendAuthCode(phone: String, completion: Params_three_void<Network.RequestedState, [String: Any]?, Int>) {
        request(request: SampleAPI.sendAuthCode(phone: phone), completion: completion)
    }
    
    /// 示例：登陆
    func sample_login(phone: String, authCode: String, completion: Params_three_void<Network.RequestedState, [String: Any]?, Int>) {
        request(request: SampleAPI.login(phone: phone, authCode: authCode), completion: completion)
    }
    
}
