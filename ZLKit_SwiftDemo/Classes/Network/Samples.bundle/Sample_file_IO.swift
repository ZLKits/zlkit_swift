//
//  Sample_file_IO.swift
//  ZLKit_Swift
//
//  Created by itzhaolei on 2022/1/18.
//

import Alamofire

// 示例：演示下载/上传
enum Sample_file_IO: URLRequestConvertible {
    
    case download(url: String)
    case upload(url: String)
    
    func asURLRequest() -> URLRequest {
        let url: URL = {
            let thisUrl: URL
            switch self {
            case .download(let url):
                thisUrl = URL.init(string: url) ?? .init(fileURLWithPath: "")
            case .upload(let url):
                thisUrl = URL.init(string: url) ?? .init(fileURLWithPath: "")
            }
            return thisUrl
        }()
        
        var method: HTTPMethod {
            switch self {
            case .download:
                return .get
            case .upload:
                return .put
            }
        }
        
        let params: ([String: Any]?) = {
            switch self {
            case .download:
                return nil
            case .upload:
                return nil
            }
        }()
        
        return request(url: url, method: method, allHTTPHeaderFields: [:], params: params)
    }
}

// 示例
public extension Network.Download {
    
    /// 示例：下载文件
    func sample_download(fromUrlString: String, toPath: String, identifier: String, completion: Params_three_void<Network.RequestedState, String, String?>) {
        request(from: Sample_file_IO.download(url: fromUrlString), toPath: toPath, identifier: identifier, completion: completion)
    }
    
}

// 示例
public extension Network.Upload {

    /// 示例：上传文件
    func sample_upload(fromPath: String, toUrlString: String, identifier: String, completion: Params_three_void<Network.RequestedState, String, DefaultDataResponse?>) {
        request(fromPath: fromPath, to: Sample_file_IO.upload(url: toUrlString), identifier: identifier, completion: completion)
    }
    
}
