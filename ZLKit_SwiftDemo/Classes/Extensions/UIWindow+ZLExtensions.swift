//
//  CGFloat+ZLExtensions.swift
//  ZLKit_SwiftDemo_Example
//
//  Created by itzhaolei on 2022/1/17.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

public extension UIWindow {
    
    /// 获取window
    static func getWindow() -> UIWindow {
        if let safeWindow = UIApplication.shared.delegate?.window as? UIWindow {
            return safeWindow
        }
        if let safeWindow = UIApplication.shared.windows.first {
            return safeWindow
        }
        if let safeWindow = UIApplication.shared.keyWindow {
            return safeWindow
        }
        return .init()
    }
    
}
