//
//  Dictionary+ZLExtensions.swift
//  ZLKit_Swift
//
//  Created by itzhaolei on 2022/1/18.
//

import Foundation

public extension Dictionary {
    
    /// 字典转json字符串
    func toString() -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: self, options: []) else { return nil }
        return .init(data: data, encoding: .utf8)
    }
    
    /// 从main.bundle里加载json转换成数组
    ///
    /// 不用带文件类型，只提供名称就行，内部自动拼接文件类型
    static func inMainBundle(forJsonName: String) -> [String : Any]? {
        let path = Bundle.main.path(forResource: "\(forJsonName).json", ofType: nil)
        let data = NSData(contentsOfFile: path ?? "") ?? .init()
        let dataStr = (NSString(data: data as Data, encoding: String.Encoding.utf8.rawValue) ?? "") as String
        return dataStr.toContainers() as? [String : Any] ?? nil
    }
    
}
