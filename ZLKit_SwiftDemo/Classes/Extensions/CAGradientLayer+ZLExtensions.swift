//
//  CAGradientLayer+ZLExtensions.swift
//  ZLKit_Swift
//
//  Created by itzhaolei on 2022/1/18.
//


// 渐变色
public extension CAGradientLayer {
    
    enum GradientPoint {
        case leftTop
        case leftBottom
        case rightTop
        case rightBottom
    }
    
    convenience init(frame: CGRect, colors:[CGColor], start: GradientPoint, end: GradientPoint) {
        self.init()
        self.frame = frame
        self.colors = colors
        switch start {
        case .leftTop:
            startPoint = CGPoint(x: 0, y: 0)
        case .leftBottom:
            startPoint = CGPoint(x: 0, y: 1)
        case .rightTop:
            startPoint = CGPoint(x: 1, y: 0)
        case .rightBottom:
            startPoint = CGPoint(x: 1, y: 1)
        }
        switch end {
        case .leftTop:
            endPoint = CGPoint(x: 0, y: 0)
        case .leftBottom:
            endPoint = CGPoint(x: 0, y: 1)
        case .rightTop:
            endPoint = CGPoint(x: 1, y: 0)
        case .rightBottom:
            endPoint = CGPoint(x: 1, y: 1)
        }
    }
    
}
