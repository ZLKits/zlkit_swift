//
//  CGFloat+ZLExtensions.swift
//  ZLKit_SwiftDemo_Example
//
//  Created by itzhaolei on 2022/1/17.
//  Copyright © 2022 CocoaPods. All rights reserved.
//


/// 日期格式化器
public func dateFormatter(format: String = "yyyy-MM-dd HH:mm:ss") -> DateFormatter {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.init(identifier: "zh_Hans_CN")
    dateFormatter.calendar = Calendar.init(identifier: .iso8601)
    dateFormatter.dateFormat = format
    dateFormatter.timeZone = .autoupdatingCurrent
    return dateFormatter
}

/// 日期格式化器
public func dateFormatter_RFC3339() -> DateFormatter {
    dateFormatter(format: "yyyy-MM-dd'T'HH:mm:ssZZZZZ")
}
