//
//  Array+ZLExtensions.swift
//  ZLKit_Swift
//
//  Created by itzhaolei on 2022/1/18.
//


public extension Array {
    
    /// 字典转json字符串
    func toString() -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: self, options: []) else { return nil }
        return .init(data: data, encoding: .utf8)
    }
    
    /// 从main.bundle里加载json转换成数组
    static func inMainBundle(forJsonName: String) -> [Any]? {
        let path = Bundle.main.path(forResource: "\(forJsonName).json", ofType: nil)
        let data = NSData(contentsOfFile: path ?? "") ?? .init()
        let dataStr = (NSString(data: data as Data, encoding: String.Encoding.utf8.rawValue) ?? "") as String
        return dataStr.toContainers() as? [Any] ?? nil
    }
    
}
