//
//  CGFloat+ZLExtensions.swift
//  ZLKit_SwiftDemo_Example
//
//  Created by itzhaolei on 2022/1/17.
//  Copyright © 2022 CocoaPods. All rights reserved.
//


/// 发送通知
public func postNotification(_ name: NSNotification.Name, userInfo: [AnyHashable : Any]? = nil) {
    NotificationCenter.default.post(name: name, object: nil, userInfo: userInfo)
}
/// 注册通知
/// - Parameters:
///   - observer: 监听者
///   - selector: 响应事件
public func addNotification(name: NSNotification.Name, observer: Any, selector: Selector) {
    NotificationCenter.default.addObserver(observer, selector: selector, name: name, object: nil)
}

/// 移除通知
public func removeNotification(name: NSNotification.Name, observer: Any) {
    NotificationCenter.default.removeObserver(observer, name: name, object: nil)
}
