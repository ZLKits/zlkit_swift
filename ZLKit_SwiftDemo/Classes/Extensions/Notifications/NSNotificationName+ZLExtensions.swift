//
//  NSNotificationName+ZLExtensions.swift
//  ZLKit_Swift
//
//  Created by itzhaolei on 2022/1/18.
//


public extension NSNotification.Name {
    
    // 程序加载完成
    static let applicationDidFinishLaunching: NSNotification.Name = UIApplication.didFinishLaunchingNotification
    
    /// 程序进入前台
    static let applicationDidBecomeActive: NSNotification.Name = UIApplication.didBecomeActiveNotification
    
    /// 程序进入后台
    static let applicationDidEnterBackground: NSNotification.Name = UIApplication.didEnterBackgroundNotification
    
    /// 内存发生警告
    static let applicationDidReceiveMemoryWarning: NSNotification.Name = UIApplication.didReceiveMemoryWarningNotification
    
    /// 键盘发生改变
    static let keyboardFrameChaged: NSNotification.Name = UIResponder.keyboardDidChangeFrameNotification
    
    /// 设备方向改变
    static let deviceOrientationDidChange: NSNotification.Name = UIDevice.orientationDidChangeNotification
    
    /// 本地化语言改变
    static let currentLocaleDidChange: NSNotification.Name = NSLocale.currentLocaleDidChangeNotification
    
}


public extension NSNotification.Name {
    
    // 用户私有数据发生改变
    static let userPrivateData: NSNotification.Name = .init("ZLKit_Swift_UserPrivateData_Changed")
    
    // 设备网络发生变化
    static let deviceNetworkChanged: NSNotification.Name = .init("ZLKit_Swift_Device_Network_Changed")
    
    // 输入框文字发生变化时
    static let textFieldTextDidChange: NSNotification.Name = .init("UITextFieldTextDidChangeNotification")
    
}
