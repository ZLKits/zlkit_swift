//
//  CGFloat+ZLExtensions.swift
//  ZLKit_SwiftDemo_Example
//
//  Created by itzhaolei on 2022/1/17.
//  Copyright © 2022 CocoaPods. All rights reserved.
//


public extension CGFloat {
    
    /// 标签栏高度
    static var tabBarHeight: CGFloat {
        return .isBangDevice ? 83.0 : 49.0
    }
    
    /// 导航栏高度
    static var navHeight: CGFloat {
        return .isBangDevice ? 84.0 : 64.0
    }
    
    /// 底部返回条的安全区域
    static var homeBarSafeInset: CGFloat {
        return .isBangDevice ? 30.0 : 0
    }
    
    /// 模态下的导航栏高度
    static var presentNavHeight: CGFloat {
        return 64.0
    }
    
    /// 底部事件条动态适配高度
    /// - Parameter contentHeight: 内容高度
    /// - Returns: 动态总高度
    static func bottomFunctionBarHeight(contentHeight: CGFloat = 50.0) -> CGFloat {
        return 10 + contentHeight + (Bool.isBangDevice ? homeBarSafeInset : 10)
    }
    
}


public extension Bool {
    
    /// 是否是齐刘海设备
    static var isBangDevice: Bool {
        var bangDevice:Bool = false
        if #available(iOS 11.0, *) {
            if UIWindow.getWindow().safeAreaInsets.bottom > 0 {
                bangDevice = true
            }
        }
        return bangDevice
    }
    
}
