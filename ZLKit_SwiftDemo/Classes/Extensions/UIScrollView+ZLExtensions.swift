//
//  UIScrollView+ZLExtensions.swift
//  ZLKit_Swift
//
//  Created by itzhaolei on 2022/1/18.
//

import Foundation

public extension UIScrollView {
    
    /// 清理配置
    func clear() {
        if self.classForCoder == UITableView.classForCoder() {
            let tableView = self as? UITableView ?? .init()
            //配置
            if #available(iOS 11.0, *) {
                contentInsetAdjustmentBehavior = .never
                scrollIndicatorInsets = contentInset
                tableView.estimatedRowHeight = 50
                tableView.estimatedSectionHeaderHeight = 0
                tableView.estimatedSectionFooterHeight = 0
            }
            if #available(iOS 15.0, *) {
                tableView.sectionHeaderTopPadding = 0
            }
            showsVerticalScrollIndicator = false
            showsHorizontalScrollIndicator = false
            tableView.separatorColor = .clear
            alwaysBounceVertical = false
            alwaysBounceHorizontal = false
            tableView.rowHeight = UITableView.automaticDimension
            tableView.sectionHeaderHeight = 0
            tableView.sectionFooterHeight = 0
            tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 0.01))
            tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 0.01))
            return
        }
        //配置
        if #available(iOS 11.0, *) {
            contentInsetAdjustmentBehavior = .never
            scrollIndicatorInsets = contentInset
        }
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
        alwaysBounceVertical = false
        alwaysBounceHorizontal = false
    }
    
    /// 清理表非自动布局下的配置
    func clearTableNotAutoLayout() {
        if self.classForCoder == UITableView.classForCoder() {
            let tableView = self as? UITableView ?? .init()
            //配置
            if #available(iOS 11.0, *) {
                contentInsetAdjustmentBehavior = .never
                scrollIndicatorInsets = contentInset
                tableView.estimatedRowHeight = 0
                tableView.estimatedSectionHeaderHeight = 0
                tableView.estimatedSectionFooterHeight = 0
            }
            if #available(iOS 15.0, *) {
                tableView.sectionHeaderTopPadding = 0
            }
            showsVerticalScrollIndicator = false
            showsHorizontalScrollIndicator = false
            tableView.separatorColor = .clear
            alwaysBounceVertical = false
            alwaysBounceHorizontal = false
            tableView.sectionHeaderHeight = 0
            tableView.sectionFooterHeight = 0
            tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 0.01))
            tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 0.01))
            return
        }
        //配置
        if #available(iOS 11.0, *) {
            contentInsetAdjustmentBehavior = .never
            scrollIndicatorInsets = contentInset
        }
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
        alwaysBounceVertical = false
        alwaysBounceHorizontal = false
    }
    
}
