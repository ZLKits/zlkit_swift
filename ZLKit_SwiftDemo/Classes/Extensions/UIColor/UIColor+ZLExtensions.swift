//
//  CGFloat+ZLExtensions.swift
//  ZLKit_SwiftDemo_Example
//
//  Created by itzhaolei on 2022/1/17.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation

public extension UIColor {
    
    /// 随机色
    static var random:UIColor { return UIColor(red: CGFloat(Float(arc4random()%255)/255.0), green: CGFloat(Float(arc4random()%255)/255.0), blue: CGFloat(Float(arc4random()%255)/255.0), alpha: 1.0) }
    
    // 基本纯色
    static let hex_000000: UIColor = .init(hexString: "#000000")
    static let hex_111111: UIColor = .init(hexString: "#111111")
    static let hex_222222: UIColor = .init(hexString: "#222222")
    static let hex_333333: UIColor = .init(hexString: "#333333")
    static let hex_444444: UIColor = .init(hexString: "#444444")
    static let hex_555555: UIColor = .init(hexString: "#555555")
    static let hex_666666: UIColor = .init(hexString: "#666666")
    static let hex_777777: UIColor = .init(hexString: "#777777")
    static let hex_888888: UIColor = .init(hexString: "#888888")
    static let hex_999999: UIColor = .init(hexString: "#999999")
    static let hex_AAAAAA: UIColor = .init(hexString: "#AAAAAA")
    static let hex_BBBBBB: UIColor = .init(hexString: "#BBBBBB")
    static let hex_CCCCCC: UIColor = .init(hexString: "#CCCCCC")
    static let hex_DDDDDD: UIColor = .init(hexString: "#DDDDDD")
    static let hex_EEEEEE: UIColor = .init(hexString: "#EEEEEE")
    static let hex_FFFFFF: UIColor = .init(hexString: "#FFFFFF")
    
    static let hex_EBEBEB: UIColor = .init(hexString: "#EBEBEB")
    static let hex_A8A8A8: UIColor = .init(hexString: "#A8A8A8")
    static let hex_364883: UIColor = .init(hexString: "#364883")
    static let hex_D1D1D1: UIColor = .init(hexString: "#D1D1D1")
    static let hex_94C6FF: UIColor = .init(hexString: "#94C6FF")
    static let hex_6CF5C5: UIColor = .init(hexString: "#6CF5C5")
    
}

public extension UIColor {
    
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
}

