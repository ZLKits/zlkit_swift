//
//  CGFloat+ZLExtensions.swift
//  ZLKit_SwiftDemo_Example
//
//  Created by itzhaolei on 2022/1/17.
//  Copyright © 2022 CocoaPods. All rights reserved.
//


public extension UIFont {
    
    /// 平方字体【系统自带字体】
    /// - Parameters:
    ///   - ofSize: 字体大小
    ///   - weight: 字重
    /// - Returns: 处理下文
    static func pingFangSC(ofSize: CGFloat, weight: PingFangSCWeight) -> UIFont {
        return UIFont.init(name: "PingFangSC-\(weight.rawValue)", size: ofSize) ?? UIFont.systemFont(ofSize: ofSize, weight: PingFangSCWeight.switchSystemWeight(weight))
    }
    
    /// 平方字重
    enum PingFangSCWeight: String {
        /// 常规
        case regular = "Regular"
        /// 特细
        case ultralight = "Ultralight"
        /// 纤细
        case thin = "Thin"
        /// 细体
        case light = "Light"
        /// 中等
        case medium = "Medium"
        /// 粗体
        case bold = "Semibold"
        
        /// 从平方字体字重转到系统字体字重
        ///
        /// 【尽量避免使用该方法进行转换】
        /// - Parameter weight: 字重
        /// - Returns: 显示结果略有出入
        static func switchSystemWeight(_ weight: PingFangSCWeight) -> UIFont.Weight {
            var safeWeight: UIFont.Weight = .regular
            switch weight {
            case ultralight:
                safeWeight = .ultraLight
                break
            case thin:
                safeWeight = .thin
                break
            case light:
                safeWeight = .light
                break
            case medium:
                safeWeight = .medium
                break
            case bold:
                safeWeight = .semibold
                break
            default:
                safeWeight = .regular
                break
            }
            return safeWeight
        }
    }
    
}
