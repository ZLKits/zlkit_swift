//
//  String_PhoneImplicit_ZLExtensions.swift
//  ZLKit_Swift
//
//  Created by 赵磊 on 2022/8/20.
//

import Foundation

public extension String {

    /// 隐式电话
    func phoneImplicit() -> Self {
        guard self.count > 6 else { return self }
        return ns.replacingCharacters(in: .init(location: 3, length: 4), with: "****")
    }
    
}
