//
//  String_Encoding_ZLExtensions.swift
//  ZLKit_Swift
//
//  Created by 赵磊 on 2022/8/20.
//

import Foundation

// 加密、解密
public extension String {
    
    /// 链接编码
    func URLChineseEncoding() -> String {
        return ns.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
    }
    
    /// 解除中文编码
    func URLChineseDecoding() -> String {
        return removingPercentEncoding ?? ""
    }
    
    /// 编码
    var base64Encoding: String {
        data(using: .utf8)?.base64EncodedString(options: .init(rawValue: 0)) ?? ""
    }
    
    /// 解码
    var base64Decoding: String {
        String(data: Data(base64Encoded: self, options: .init(rawValue: 0)) ?? .init(), encoding: .utf8) ?? ""
    }
    
}
