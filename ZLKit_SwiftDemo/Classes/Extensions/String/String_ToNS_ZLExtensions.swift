//
//  String_ToNS_ZLExtensions.swift
//  ZLKit_Swift
//
//  Created by 赵磊 on 2022/8/20.
//

import Foundation

public extension String {
    
    var ns: NSString {
        return self as NSString
    }
    
}


