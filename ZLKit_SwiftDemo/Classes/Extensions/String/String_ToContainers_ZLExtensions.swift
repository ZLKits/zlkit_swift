//
//  String_ToContainers_ZLExtensions.swift
//  ZLKit_Swift
//
//  Created by 赵磊 on 2022/8/20.
//

import Foundation

public extension String {
    
    /// json字符串转容器
    func toContainers() -> Any? {
        let data = data(using: .utf8) ?? .init()
        return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    }
    
}
