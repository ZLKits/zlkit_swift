//
//  CGFloat+ZLExtensions.swift
//  ZLKit_SwiftDemo_Example
//
//  Created by itzhaolei on 2022/1/17.
//  Copyright © 2022 CocoaPods. All rights reserved.
//



public extension UIImage {
    
    class AssistObject: NSObject { }
    
    static var goBack: UIImage? {
        return .pathFromZLKit(image_name: "goback.png", file_bundle_name: "Extensions")
    }
    
    static var close: UIImage? {
        return .pathFromZLKit(image_name: "close.png", file_bundle_name: "Extensions")
    }
    
    static var next: UIImage? {
        return .pathFromZLKit(image_name: "next.png", file_bundle_name: "Extensions")
    }
    
    /// 从当前库里加载图片
    /// - Parameters:
    ///   - image_name: 文件全名
    ///   - file_bundle_name: 图片所在的bundle包名（最近的bundle）
    static func pathFromZLKit(image_name: String, file_bundle_name: String) -> UIImage? {
        let bundle = Bundle(for: AssistObject.classForCoder())
        let path = "\(bundle.bundlePath)/\(file_bundle_name).bundle/\(file_bundle_name).bundle/\(image_name)"
        return .init(contentsOfFile: path)
    }
    
    /// 加载系统图片
    @available(iOS 13.0, *)
    static func system(name: String, Size size:CGFloat, Color color: UIColor = .init(hexString: "333333")) -> UIImage {
        let smallConfig = UIImage.SymbolConfiguration(scale: .small)
        let boldSmallConfig = UIImage.SymbolConfiguration(pointSize: size, weight: .regular, scale: .large).applying(smallConfig)
        return UIImage(systemName: name, withConfiguration: boldSmallConfig)?.withTintColor(color, renderingMode: .alwaysOriginal) ?? .init()
    }
    
}
