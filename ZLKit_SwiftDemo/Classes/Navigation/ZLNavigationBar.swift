//
//  ZLNavigationBar.swift
//  ZLKit_SwiftDemo_Example
//
//  Created by itzhaolei on 2022/1/18.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import SnapKit

public enum ZLNavigation { }

public extension ZLNavigation {
    
    class Bar: UIView {
        
        open var title: String? = nil {
            didSet {
                
            }
        }
        
        open override var backgroundColor: UIColor? {
            didSet {
                
            }
        }
        
        open var backgroundImage: UIImage? = nil {
            didSet {
                
            }
        }
        
        open var leftImage: UIImage? = nil {
            didSet {
                
            }
        }
        
        open var leftItems: [UIImage]? = nil {
            didSet {
                
            }
        }
        
        open var rightItem: UIImage? = nil {
            didSet {
                
            }
        }
        
        open var rightItems: [UIImage]? = nil {
            didSet {
                
            }
        }
        
        /// 背景条
        lazy var backgroundBar: BackgroundBar  = {
            let view = BackgroundBar()
            return view
        }()
        
        /// 事件条
        lazy var actionBar: ActionBar  = {
            let view = ActionBar()
            return view
        }()
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            addSubviewsConstraints()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        open func addSubviews() {
            addSubview(backgroundBar)
            addSubview(actionBar)
        }
        
        open func addSubviewsConstraints() {
            backgroundBar.snp.makeConstraints({
                $0.edges.equalToSuperview()
            })
            actionBar.snp.makeConstraints({
                $0.leading.trailing.equalToSuperview()
                $0.top.equalToSuperview().inset(CGFloat.navHeight - 40.0 - 4.0)
                $0.height.equalTo(40)
            })
        }
        
        /// 常规的约束
        open func normalConstraints() {
            snp.makeConstraints({
                $0.leading.trailing.top.equalToSuperview()
                $0.height.equalTo(CGFloat.navHeight)
            })
        }
        
    }

    
}
